# GitLab API Python

> Python wrapper and CLI for the GitLab REST API.

**At this point, this code is nothing more than me experimenting. If you need a GitLab REST API CLI or wrapper, go to ["Applications Supporting Gitlab" (Api Client)](https://about.gitlab.com/applications/) page and pick a client.**

My goal with this project is to practice using [type hints](https://www.python.org/dev/peps/pep-0484/) in Python. I'd like to see how type hints affect developer productivity and the maintainability of the code.

## Usage

You just don't. Not yet.

